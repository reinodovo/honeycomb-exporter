package honeycomb

import (
	"context"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	"google.golang.org/grpc/credentials"
)

type Options struct {
	ServiceName string
	ApiKey      string
	Dataset     string
}

type HoneycombExporter struct {
	tp *sdktrace.TracerProvider
}

func StartHoneycombExporter(ctx context.Context, options Options) (*HoneycombExporter, error) {
	exp, err := newExporter(ctx, options)
	if err != nil {
		return nil, err
	}

	exporter := &HoneycombExporter{
		tp: newTraceProvider(exp, options),
	}

	otel.SetTracerProvider(exporter.tp)

	otel.SetTextMapPropagator(
		propagation.NewCompositeTextMapPropagator(
			propagation.TraceContext{},
			propagation.Baggage{},
		),
	)

	return exporter, nil
}

func (exporter *HoneycombExporter) Shutdown(ctx context.Context) error {
	return exporter.tp.Shutdown(ctx)
}

func newExporter(ctx context.Context, options Options) (*otlptrace.Exporter, error) {
	opts := []otlptracegrpc.Option{
		otlptracegrpc.WithEndpoint("api.honeycomb.io:443"),
		otlptracegrpc.WithHeaders(map[string]string{
			"x-honeycomb-team":    options.ApiKey,
			"x-honeycomb-dataset": options.Dataset,
		}),
		otlptracegrpc.WithTLSCredentials(credentials.NewClientTLSFromCert(nil, "")),
	}

	client := otlptracegrpc.NewClient(opts...)
	return otlptrace.New(ctx, client)
}

func newTraceProvider(exp *otlptrace.Exporter, options Options) *sdktrace.TracerProvider {
	resource :=
		resource.NewWithAttributes(
			semconv.SchemaURL,
			semconv.ServiceNameKey.String(options.ServiceName),
		)

	return sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(resource),
	)
}
